const carouselSlide = document.querySelector('.carousel-slide');//querySelector - used when collecting only one element
const carouselImages = document.querySelectorAll('.carousel-slide img');//querySelectorAll - used when collecting multiple elements

//Buttons
const prevBtn = document.querySelector('#prevBtn');
const nextBtn = document.querySelector('#nextBtn');

//counter
let counter = 1;

/**
 *clientWidth - returns the viewable width of an element in pixels, including padding but not border, scrollbar or margin
 * The reason the word viewable is used is because if the element's content is wider than the actual width of the element, this property will only return the width that is visible
 */
const size =  carouselImages[0].clientWidth;//the size variable is equal to the width of the first image

carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';// this slides the image along the x axis for the number of pixels specified, in this case -size*counter


//Button Listeners

/**
 * When the next button is clicked the program checks whether there are other slides and if not it jumps out.
 * Otherwise it adds a transition to the transform function created earlier to add effects to the image transformation
 * increase the counter by one
 * then applies a new transform function with an updated pixel value due to the new counter number, slides the width(size) of the number of images (counter) out of the way.
 */

nextBtn.addEventListener('click', () => {
    if (counter >= carouselImages.length -1) return;// if the counter is greater than carousel.length -1 (this is pointing to the first image clone) then just return and don't execute the rest of the code
    carouselSlide.style.transition = "transform 0.4s ease-in-out";//adding transition styling to the transform element
    counter++;
    console.log(counter);
    carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';

});

prevBtn.addEventListener('click', () => {
    if(counter <= 0) return;
    carouselSlide.style.transition = 'transform 0.4s ease-in-out';
    counter--;
    carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';

});
/**
 * At the end of the transition it checks if the current id is the clone of the last image and if so it stops the transition and the uses translate to reposition back to the original image
 *
 * it also checks for the clone of the first image and performs the same action
 * this give the effect of the never ending slideshow
 */
carouselSlide.addEventListener('transitionend', () =>{
    //console.log(carouselImages[counter]);
    if (carouselImages[counter].id === 'lastClone') {
        carouselSlide.style.transition = 'none';
        counter = carouselImages.length - 2;
        carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
    }

    if (carouselImages[counter].id === 'firstClone'){
        carouselSlide.style.transition = 'none';
        counter = carouselImages.length - counter;
        carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
    }
});